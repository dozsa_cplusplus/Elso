#include <iostream>
#include <algorithm>
#include <cstdlib>

int osszeadas(int a, int b){
    return (a + b);
}

float osztas(int a, int b){
    return (a / b);
}

int kivonas(int a, int b){
    return a - b;
}

int intInput(std::string inputString){
    int n;
    std::string tmp;
    bool success;

    do {
        std::cout << inputString;
        std::cin >> tmp;
        std::cout << std::endl;
        n = atoi(tmp.c_str());
        success = (n == 0 && tmp != "0");
    }while(success);
    return n;
}

std::string toUppercase(std::string &str){
    std::transform(str.begin(), str.end(), str.begin(), ::toupper);
}

enum stringCode {
    INPUT_OA,
    INPUT_OSZ,
    INPUT_K
};

stringCode hashit (const std::string &inString) {
    if (inString == "OA") return INPUT_OA;
    if (inString == "OSZ") return INPUT_OSZ;
    if (inString == "K") return INPUT_K;
}

int main()
{
    int a;
    int b;
    std::string szoveg;

    std::cout << "Hello world!" << std::endl;
    std::cout << "Valasszon muveletet [OA,OSZ,K]: ";
    std::cin >> szoveg;

    toUppercase(szoveg);
    a = intInput("Szam A: ");
    b = intInput("Szam B: ");

    switch(hashit(szoveg)){
    case INPUT_OA:
        std::cout << "Vegeredmeny: " << osszeadas(a, b) << std::endl;
        break;
    case INPUT_OSZ:
        std::cout << "Vegeredmeny: " << osztas(a, b) << std::endl;
        break;
    case INPUT_K:
        std::cout << "Vegeredmeny: " << kivonas(a, b) << std::endl;
        break;
    default:
        std::cout << "Hibas parameter" << std::endl;
        break;
    }
    return 0;
}
